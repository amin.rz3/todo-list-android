package amin.rz3.todolist.detail;

import amin.rz3.todolist.BasePresenter;
import amin.rz3.todolist.BaseView;
import amin.rz3.todolist.model.Task;

public interface TaskDetailContract {

    interface View extends BaseView {
        void showTask(Task task);

        void setDeleteButtonVisibility(boolean visibility);

        void showError(String error);

        void returnResult(int resultCode, Task task);
    }

    interface Presenter extends BasePresenter<View> {
        void deleteTask();

        void saveChanges(int importance, String title);

    }

}
