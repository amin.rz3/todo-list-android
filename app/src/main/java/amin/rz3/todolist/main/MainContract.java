package amin.rz3.todolist.main;

import java.util.List;

import amin.rz3.todolist.BasePresenter;
import amin.rz3.todolist.BaseView;
import amin.rz3.todolist.model.Task;

public interface MainContract {

    interface View extends BaseView {
        void showTasks(List<Task> tasks);

        void clearTasks();

        void updateTask(Task task);

        void setEmptyStateVisibility(boolean visibility);
    }

    interface Presenter extends BasePresenter<View> {
        void onDeleteAllBtnClick();

        void onSearch(String q);

        void onTaskItemClick(Task task);
    }
}
